﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hw20
{
    public partial class Form1 : Form
    {
        
        private static Mutex cardMut = new Mutex(); // mutex for chosenCard

        private static string chosenCard = ""; //the card code (shared resource)

        private static int playerScore = 0;
        private static int OpponentScore = 0;

        private static PictureBox chosenCardPic = null; // the picture box of the card that was choosen by the user (one of the 10 generated cards)

        private static NetworkStream clientStream; 

        //                   key - card code     value - card image
        private static Dictionary<string, Bitmap> cards = new Dictionary<string, Bitmap>
        {
            {"01C",Properties.Resources.ace_of_clubs},
            {"01D",Properties.Resources.ace_of_diamonds},
            {"01H",Properties.Resources.ace_of_hearts},
            {"01S",Properties.Resources.ace_of_spades2},

            {"02C",Properties.Resources._2_of_clubs},
            {"02D",Properties.Resources._2_of_diamonds},
            {"02H",Properties.Resources._2_of_hearts},
            {"02S",Properties.Resources._2_of_spades},

            {"03C",Properties.Resources._3_of_clubs},
            {"03D",Properties.Resources._3_of_diamonds},
            {"03H",Properties.Resources._3_of_hearts},
            {"03S",Properties.Resources._3_of_spades},

            {"04C",Properties.Resources._4_of_clubs},
            {"04D",Properties.Resources._4_of_diamonds},
            {"04H",Properties.Resources._4_of_hearts},
            {"04S",Properties.Resources._4_of_spades},

            {"05C",Properties.Resources._5_of_clubs},
            {"05D",Properties.Resources._5_of_diamonds},
            {"05H",Properties.Resources._5_of_hearts},
            {"05S",Properties.Resources._5_of_spades},

            {"06C",Properties.Resources._6_of_clubs},
            {"06D",Properties.Resources._6_of_diamonds},
            {"06H",Properties.Resources._6_of_hearts},
            {"06S",Properties.Resources._6_of_spades},

            {"07C",Properties.Resources._7_of_clubs},
            {"07D",Properties.Resources._7_of_diamonds},
            {"07H",Properties.Resources._7_of_hearts},
            {"07S",Properties.Resources._7_of_spades},

            {"08C",Properties.Resources._8_of_clubs},
            {"08D",Properties.Resources._8_of_diamonds},
            {"08H",Properties.Resources._8_of_hearts},
            {"08S",Properties.Resources._8_of_spades},

            {"09C",Properties.Resources._9_of_clubs},
            {"09D",Properties.Resources._9_of_diamonds},
            {"09H",Properties.Resources._9_of_hearts},
            {"09S",Properties.Resources._9_of_spades},

            {"10C",Properties.Resources._10_of_clubs},
            {"10D",Properties.Resources._10_of_diamonds},
            {"10H",Properties.Resources._10_of_hearts},
            {"10S",Properties.Resources._10_of_spades},

            {"11C",Properties.Resources.jack_of_clubs2},
            {"11D",Properties.Resources.jack_of_diamonds2},
            {"11H",Properties.Resources.jack_of_hearts2},
            {"11S",Properties.Resources.jack_of_spades2},

            {"12C",Properties.Resources.queen_of_clubs2},
            {"12D",Properties.Resources.queen_of_diamonds2},
            {"12H",Properties.Resources.queen_of_hearts2},
            {"12S",Properties.Resources.queen_of_spades2},

            {"13C",Properties.Resources.king_of_clubs2},
            {"13D",Properties.Resources.king_of_diamonds2},
            {"13H",Properties.Resources.king_of_hearts2},
            {"13S",Properties.Resources.king_of_spades2}
        };

        //----------------------EVENT FUNCTIONS
        public Form1()
        {
            InitializeComponent();

            //disabling all componants until opponent connects
            foreach (Control con in this.Controls)
            {
                con.Enabled = false;
            }

            //starting the server connection thread
            Thread ServerThread = new Thread(ServerHanle);
            ServerThread.IsBackground = true;
            ServerThread.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
 
        //gererates a random card, reveals the card to the player and puts it in the 'chosedCard' string
        private void Card_Click(object sender, EventArgs e)
        {
            //make sure every card is face down
            CleanCards();

            //generate random card
            string[] cardList = cards.Keys.ToArray();
            chosenCardPic = (PictureBox)sender;
            Random r = new Random();

            //set chosenCard
            cardMut.WaitOne();
            chosenCard = cardList[r.Next(0, cardList.Length - 1)];
            cardMut.ReleaseMutex();

            //changing the card on screen to the chosen card
            ((PictureBox)sender).Image = cards[chosenCard];
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if the user pressed close
            if (e.CloseReason.ToString().Equals("UserClosing"))
            {
                ExitProgram();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            ExitProgram();
        }

        //----------------------OTHER FUNCTIONS

        //----creates 10 cards on the form
        private void GenerateCards()
        {
            const int padding = 10;
            const int startingX = 10;
            const int startingY = 280;
            const int cardWidth = 90;
            const int cardHeight = 130;

            Point nextPos = new Point(startingX, startingY);

            //creating 10 cards
            for (int i = 0; i < 10; i++)
            {
                //creating a card
                System.Windows.Forms.PictureBox currCard = new PictureBox
                {
                    Name = "PlayerCard" + i,
                    Image = global::hw20.Properties.Resources.card_back_blue,
                    Location = nextPos,
                    Size = new System.Drawing.Size(cardWidth, cardHeight),
                    SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
                };
                currCard.Click += Card_Click;
                //assigning it a function


                //adding the card picture
                this.Controls.Add(currCard);

                //changing nextPos
                nextPos.X += cardWidth + padding;

            }

        }

        //Sends an ending game message to the server and exits the application 
        private void ExitProgram()
        {
            //showing score message
            SendMsgToServer("2000");
            string msg = "Your score: " + playerScore + "\nOpponent score: " + OpponentScore ;
            MessageBox.Show(msg);
            //exiting the program
            Application.Exit();
        }

        //----comunacation with the server <background thread>
        private void ServerHanle(object obj)
        {
            const int SERVER_PORT = 8820;
            const string SERVER_IP = "127.0.0.1";

            //connecting to the server
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(SERVER_IP), SERVER_PORT);
            try
            {
                client.Connect(serverEndPoint);
            }
            catch (Exception e)
            {
                //if connection failed
                MessageBox.Show(e.Message);
                Environment.Exit(1);
            }

            clientStream = client.GetStream();

            //waiting for the statring message from the server
            byte[] bufferIn = new byte[4];
            try
            {
                clientStream.Read(bufferIn, 0, 4);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Environment.Exit(1);
            }

            string Serverinput = new ASCIIEncoding().GetString(bufferIn);

            //if message is 0000 then start the game
            if (Serverinput.Equals("0000"))
            {
                //unfreezeing the controls
                Invoke((MethodInvoker)delegate
                {
                    foreach (Control con in this.Controls)
                    {
                        con.Enabled = true;
                    }
                    GenerateCards();
                });
            }


            //----entering game loop
            int bytesRead = 0;
            bool gotServerMsg = false;
            bool gotClientMsg = false;

            while (true)
            {
                //reading from the stream
                if(!gotServerMsg)
                {
                    try
                    {
                        clientStream.ReadAsync(bufferIn, 0, 4);  //reading without waiting for the stream to change
                        bytesRead = bufferIn.Length;
                        Serverinput = new ASCIIEncoding().GetString(bufferIn);
                        
                        //if message is 'end game' message
                        if(Serverinput[0].Equals('2'))
                        {
                            ExitProgram();
                        }
                    }
                    catch
                    {
                        gotServerMsg = false;
                        bytesRead = 0;
                    }

                    //if card message is ok
                    if (bytesRead.Equals(4) && !Serverinput.Equals("0000") )
                    {
                        gotServerMsg = true;

                        //removing the code char
                        Serverinput = Serverinput.Substring(1);

                    }
                }


                //trying to get the card that was choosen
                cardMut.WaitOne();
                if(!chosenCard.Equals("") && !gotClientMsg)
                {
                    gotClientMsg = true;

                    //sending the card message to the server
                    SendMsgToServer("1" + chosenCard);
                }
                cardMut.ReleaseMutex();

                //when both the opponent and the user have choosen cards
                if (gotServerMsg && gotClientMsg)
                {
                    gotClientMsg = false;
                    gotServerMsg = false;

                    //reviling the card the opponent chose, looking who won and resetting the game
                    Invoke((MethodInvoker)delegate
                    {
                        if(cards.ContainsKey(Serverinput))
                        {
                            OpponentCard.Image = cards[Serverinput];
                        }
                    });
                   
                    //calculating who won the round
                    int opponentCardNum = 0;
                    int playerCardNum = 0;
                    try
                    {
                        cardMut.WaitOne();
                        int.TryParse(chosenCard.Substring(0, 2), out playerCardNum);
                        chosenCard = "";
                        cardMut.ReleaseMutex();

                        int.TryParse(Serverinput.Substring(0, 2), out opponentCardNum);
                       
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("trying to parse the cards\n" + e.Message);
                    }
                    
                    //increasing score
                    if(playerCardNum > opponentCardNum)
                    {
                        playerScore++;
                    }
                    else if(playerCardNum < opponentCardNum)
                    {
                        OpponentScore++;
                    }

                    //changing the score labels
                    Invoke((MethodInvoker)delegate
                    {
                        playerLabel.Text = "Your score: " + playerScore;
                        opponentLabel.Text = "Opponent's score: " + OpponentScore;
                    });
                    
                    
                }
            }

        }

        //sends a 4 char message to the server
        private static void SendMsgToServer(string msg)
        {
            if(msg.Length.Equals(4))
            {
                try
                {
                    //sending the message
                    byte[] buffer = new ASCIIEncoding().GetBytes(msg);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            else
            {
                MessageBox.Show("message to server not 4 chars long");
            }
        }

        //turns the fliped over cards to their backside
        private void CleanCards()
        {
            //changing the images
            if (chosenCardPic != null)
            {
                chosenCardPic.Image = Properties.Resources.card_back_blue;
                chosenCardPic = null;
            }
            OpponentCard.Image = Properties.Resources.card_back_red;
        }
    }
}
