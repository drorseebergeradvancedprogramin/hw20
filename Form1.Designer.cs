﻿namespace hw20
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpponentCard = new System.Windows.Forms.PictureBox();
            this.playerLabel = new System.Windows.Forms.Label();
            this.opponentLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.OpponentCard)).BeginInit();
            this.SuspendLayout();
            // 
            // OpponentCard
            // 
            this.OpponentCard.Image = global::hw20.Properties.Resources.card_back_red;
            this.OpponentCard.Location = new System.Drawing.Point(470, 80);
            this.OpponentCard.Name = "OpponentCard";
            this.OpponentCard.Size = new System.Drawing.Size(90, 130);
            this.OpponentCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OpponentCard.TabIndex = 0;
            this.OpponentCard.TabStop = false;
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerLabel.Location = new System.Drawing.Point(12, 38);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(108, 17);
            this.playerLabel.TabIndex = 1;
            this.playerLabel.Text = "Your Score: 0";
            // 
            // opponentLabel
            // 
            this.opponentLabel.AutoSize = true;
            this.opponentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opponentLabel.Location = new System.Drawing.Point(809, 38);
            this.opponentLabel.Name = "opponentLabel";
            this.opponentLabel.Size = new System.Drawing.Size(157, 17);
            this.opponentLabel.TabIndex = 2;
            this.opponentLabel.Text = "Opponent\'s Score: 0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(470, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Forefit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(1014, 459);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.opponentLabel);
            this.Controls.Add(this.playerLabel);
            this.Controls.Add(this.OpponentCard);
            this.Name = "Form1";
            this.Text = "Milhama!";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OpponentCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox OpponentCard;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label opponentLabel;
        private System.Windows.Forms.Button button1;
    }
}

